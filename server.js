require("./models/db");

const app = require("express")();
const path = require("path");
const exphbs = require("express-handlebars");
const bodyParser = require("body-parser");

const employeeController = require("./controllers/employeeController");

app.use(
  bodyParser.urlencoded({
    extended: true
  })
);

app.set("views", path.join(__dirname, "/views/"));
app.engine(
  "hbs",
  exphbs({
    extname: "hbs",
    defaultLayout: "mainLayout",
    layoutsDir: __dirname + "/views/layouts/"
  })
);
app.use(bodyParser.json());
app.set("view engine", "hbs");

const port = 3000;
app.listen(port, () => {
  console.log(`Server started on port ` + port);
});

app.use("/employee", employeeController);
